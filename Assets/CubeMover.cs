﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMover : MonoBehaviour
{
    private Rigidbody mRigidBody;
    public float force;
    private Vector3 moveVector;

    void Start()
    {
        mRigidBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        moveVector.x = Input.GetAxis("Vertical");
        moveVector.z = -Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        mRigidBody.velocity = moveVector * force;
    }
}