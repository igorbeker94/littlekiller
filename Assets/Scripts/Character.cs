﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class Character : MonoBehaviour, CharacterControls.ICharacterActionMapActions
{
    public static Character Instance;

    public AudioSource audioSource;
    public AudioClip missClip;
    public AudioClip hitClip;

    public Transform leftFist;
    public Transform rightFist;

    public float fistAttackY;
    public float fistRestY;

    public float fistBackSpeed;

    public float minSpeed;
    public float maxSpeed;

    public float accelerationSpeed;
    public float punchForce;

    Vector2 leftFistPosition;
    Vector2 rightFistPosition;
    Vector2 characterPosition;

    CharacterControls controls;
    Vector2 movementVector;
    Vector2 lookVector;
    float speed;

    private void Awake()
    {
        Instance = this;

        leftFistPosition = leftFist.localPosition;
        rightFistPosition = rightFist.localPosition;

        controls = new CharacterControls();
        controls.Enable();
        controls.CharacterActionMap.SetCallbacks(this);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            LeftPunch();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            RightPunch();
        }

        leftFist.localPosition = leftFistPosition;
        rightFist.localPosition = rightFistPosition;

        if (leftFistPosition.y > fistRestY)
            leftFistPosition.y -= Time.deltaTime * fistBackSpeed;
        else leftFistPosition.y = fistRestY;

        if (rightFistPosition.y > fistRestY)
            rightFistPosition.y -= Time.deltaTime * fistBackSpeed;
        else rightFistPosition.y = fistRestY;

        characterPosition += movementVector * speed * Time.deltaTime;

        transform.localPosition = characterPosition;
        if (movementVector.magnitude > 0)
        {
            if (speed < maxSpeed)
                speed += movementVector.magnitude * accelerationSpeed * Time.deltaTime;
            else speed = maxSpeed;
        }
        else if (speed > minSpeed)
            speed -= accelerationSpeed * 5 * Time.deltaTime;
        else speed = minSpeed;

        transform.localScale = Vector2.one * (((maxSpeed - speed) / 4) + 1);
        if (lookVector.magnitude > .1f)
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(-lookVector.x, lookVector.y) * Mathf.Rad2Deg);
    }

    private void LeftPunch()
    {
        leftFistPosition.y = fistAttackY;

        bool hit = TryHit();
        audioSource.PlayOneShot(hit ? hitClip : missClip);
    }

    private void RightPunch()
    {
        rightFistPosition.y = fistAttackY;
        bool hit = TryHit();
        audioSource.PlayOneShot(hit ? hitClip : missClip);
    }

    private bool TryHit()
    {
        bool result = false;

        foreach (var enemy in enemiesInRange)
        {
            Vector2 dir = enemy.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            
            if (Mathf.Abs(angle-90) < 120)
            {
                enemy.GetComponent<Enemy>().GetHit(dir * punchForce);
                result = true;
            }
        }

        return result;
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        movementVector = context.ReadValue<Vector2>();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        lookVector = context.ReadValue<Vector2>();
    }

    public void OnLeftPunch(InputAction.CallbackContext context)
    {
        if (context.started) LeftPunch();
    }

    public void OnRightPunch(InputAction.CallbackContext context)
    {
        if (context.started) RightPunch();
    }

    private List<Transform> enemiesInRange = new List<Transform>();

    private void OnTriggerEnter2D(Collider2D other)
    {
        enemiesInRange.Add(other.transform);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        enemiesInRange.Remove(other.transform);
    }
}