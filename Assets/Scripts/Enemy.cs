﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;

    Transform target;
    private Rigidbody2D mRigidBody2D;

    private void Start()
    {
        mRigidBody2D = GetComponent<Rigidbody2D>();
        target = Character.Instance.transform;
    }

    private float timer;

    void Update()
    {
        Vector2 dir = target.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        if (gotHit)
        {
            timer += Time.deltaTime;
            if (timer > .75f) gotHit = false;
        }
        else timer = 0;
    }

    private bool gotHit = false;

    public void GetHit(Vector2 dir)
    {
        gotHit = true;
        mRigidBody2D.AddForce(dir);
    }

    private void FixedUpdate()
    {
        if (gotHit) return;
        mRigidBody2D.velocity = (transform.up * speed * Time.fixedDeltaTime);
    }
}