// GENERATED AUTOMATICALLY FROM 'Assets/Input/CharacterControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CharacterControls : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @CharacterControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CharacterControls"",
    ""maps"": [
        {
            ""name"": ""CharacterActionMap"",
            ""id"": ""ff7175a0-9468-4b09-ac27-ccfc81919410"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""726d53a0-1acb-42df-a199-062777812464"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""c7be059b-a11e-4356-8ccc-e09b269fb474"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftPunch"",
                    ""type"": ""Button"",
                    ""id"": ""524bf820-dae9-4907-81d6-adef5f8ac9cd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightPunch"",
                    ""type"": ""Button"",
                    ""id"": ""8ef0db16-8db6-4e67-9a32-6fb8c7d72998"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""17233994-ba35-4f52-acf1-d46f26aa4728"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40553ab6-2baa-4d85-a87d-3b1bcf0bc764"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""070b596d-21bd-46b2-a01a-a477a1eca462"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftPunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1c8f1b35-0b96-4d21-9eff-d8d5b01df773"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightPunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // CharacterActionMap
        m_CharacterActionMap = asset.FindActionMap("CharacterActionMap", throwIfNotFound: true);
        m_CharacterActionMap_Movement = m_CharacterActionMap.FindAction("Movement", throwIfNotFound: true);
        m_CharacterActionMap_Look = m_CharacterActionMap.FindAction("Look", throwIfNotFound: true);
        m_CharacterActionMap_LeftPunch = m_CharacterActionMap.FindAction("LeftPunch", throwIfNotFound: true);
        m_CharacterActionMap_RightPunch = m_CharacterActionMap.FindAction("RightPunch", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // CharacterActionMap
    private readonly InputActionMap m_CharacterActionMap;
    private ICharacterActionMapActions m_CharacterActionMapActionsCallbackInterface;
    private readonly InputAction m_CharacterActionMap_Movement;
    private readonly InputAction m_CharacterActionMap_Look;
    private readonly InputAction m_CharacterActionMap_LeftPunch;
    private readonly InputAction m_CharacterActionMap_RightPunch;
    public struct CharacterActionMapActions
    {
        private @CharacterControls m_Wrapper;
        public CharacterActionMapActions(@CharacterControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_CharacterActionMap_Movement;
        public InputAction @Look => m_Wrapper.m_CharacterActionMap_Look;
        public InputAction @LeftPunch => m_Wrapper.m_CharacterActionMap_LeftPunch;
        public InputAction @RightPunch => m_Wrapper.m_CharacterActionMap_RightPunch;
        public InputActionMap Get() { return m_Wrapper.m_CharacterActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CharacterActionMapActions set) { return set.Get(); }
        public void SetCallbacks(ICharacterActionMapActions instance)
        {
            if (m_Wrapper.m_CharacterActionMapActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnMovement;
                @Look.started -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLook;
                @LeftPunch.started -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLeftPunch;
                @LeftPunch.performed -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLeftPunch;
                @LeftPunch.canceled -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnLeftPunch;
                @RightPunch.started -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnRightPunch;
                @RightPunch.performed -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnRightPunch;
                @RightPunch.canceled -= m_Wrapper.m_CharacterActionMapActionsCallbackInterface.OnRightPunch;
            }
            m_Wrapper.m_CharacterActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @LeftPunch.started += instance.OnLeftPunch;
                @LeftPunch.performed += instance.OnLeftPunch;
                @LeftPunch.canceled += instance.OnLeftPunch;
                @RightPunch.started += instance.OnRightPunch;
                @RightPunch.performed += instance.OnRightPunch;
                @RightPunch.canceled += instance.OnRightPunch;
            }
        }
    }
    public CharacterActionMapActions @CharacterActionMap => new CharacterActionMapActions(this);
    public interface ICharacterActionMapActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnLeftPunch(InputAction.CallbackContext context);
        void OnRightPunch(InputAction.CallbackContext context);
    }
}
